# Troubleshooting

## Code Suggestions not displayed

1. Check all the steps in [Code Suggestions aren't displayed](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/troubleshooting.html#code-suggestions-are-not-displayed) first.
1. Ensure you have properly [set up the extension](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension#setup).
1. Ensure you are working on a [supported language](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension#supported-languages).
1. If another extension provides similar suggestion or completion features, the extension might not return suggestions. To resolve this:
   1. Disable all other Visual Studio extensions.
   1. Confirm that you now receive code suggestions.
   1. Re-enable extensions one at a time, testing for code suggestions each time, to find the extension that conflicts.

## Additional logging

Additional logging can be viewed from the **GitLab Extension Output** window:

1. From the **Tools > Options** menu, find the **GitLab** option. Ensure **Log Level** is set to **Debug**.
1. Open the extension log in **View > Output** and change the dropdown list to **GitLab Extension** as the log filter.
1. Verify that the debug log contains similar output:

```shell
14:48:21:344 GitlabProposalSource.GetCodeSuggestionAsync
14:48:21:344 LsClient.SendTextDocumentCompletionAsync("GitLab.Extension.Test\TestData.cs", 34, 0)
14:48:21:346 LS(55096): time="2023-07-17T14:48:21-05:00" level=info msg="update context"
```

## Error: unable to find last release

If you receive this error message, your commits are likely on the main branch of
your fork, instead of a feature branch:

```plaintext
buildtag.sh: Error: unable to find last release.
```

To resolve this issue:

1. Create a separate feature branch for your changes.
1. Cherry-pick your commits into your feature branch.
1. Retry your command.
