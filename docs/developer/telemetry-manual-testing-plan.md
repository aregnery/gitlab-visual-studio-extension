# Code suggestions telemetry: manual testing plan

**Objective:**

Until we have automated integration tests, we must verify that changes to how code suggestion telemetry is tracked work as expected, and do not introduce any regressions. The goal of this testing is to verify that code suggestion telemetry events (shown, rejected, accepted) are being properly tracked by the extension and sent to the language server.

**Prerequisites:**

- The Visual Studio extension is installed and enabled.
- Log messages are accessible for verification. With the extension settings setting the LogLevel to be DEBUG or lower.

## Shown event

### Test scenario 1: proposal is shown

1. Open a code file in Visual Studio.
1. Trigger a code suggestion event: start typing a class or a method signature.
1. Verify that a log message with this format is generated:

   ```plaintext
   [GitLab.Extension.LanguageServer.LsClientTs] SendGitlabTelemetryCodeSuggestionShownAsync("{proposal_telemetry_tracking_id}")
   ```

1. Ensure that the proposal is shown in the UI

### Test scenario 2: proposal not shown

1. Open a code file in Visual Studio.
1. Ensure no code suggestion event is triggered.
1. Verify that no log message is generated in the format mentioned in [test scenario 1](#test-scenario-1-proposal-is-shown).

## Rejected

### Test scenario 3: proposal rejected

1. Open a code file in Visual Studio.
1. Trigger a code suggestion event.
1. Perform one of the rejection actions.
   - Press <kbd>Backspace</kbd>.
   - Press <kbd>Escape</kbd>.
   - Press <kbd>Return</kbd>.
   - Type additional text that diverges from the suggestion.
   - Type additional text that does not diverge from the suggestion.
   - Close the file.
   - Close the Visual Studio application.
1. For each rejection action, verify that a corresponding log message is generated in this format:

   ```plaintext
   [GitLab.Extension.LanguageServer.LsClientTs] SendGitlabTelemetryCodeSuggestionRejectedAsync("{proposal_telemetry_tracking_id}")`.
   ```

1. Ensure that the proposal is no longer visible in the UI after rejection.

This test should be repeated for each rejection event, as Visual Studio does treat these events differently.

## Accepted event

### Test scenario 4: proposal accepted

1. Open a code file in Visual Studio.
1. Trigger a code suggestion event.
1. Select a suggestion, then accept it by pressing <kbd>Tab</kbd>.
1. Verify that a log message with this format is generated:

   ```plaintext
   [GitLab.Extension.LanguageServer.LsClientTs] SendGitlabTelemetryCodeSuggestionAcceptedAsync("{proposal_telemetry_tracking_id}")
   ```

1. Ensure that the proposal is accepted and properly inserted into the code.
