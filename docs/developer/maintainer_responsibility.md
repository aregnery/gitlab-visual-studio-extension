---
stage: Create
group: ide
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Maintainer Responsibility

## Overview

Maintainers play a critical role in the development and upkeep of the GitLab Visual Studio Extension. As the extension evolves, maintainers are expected to fulfill a broad set of duties, ensuring the project continues to meet high standards of quality and functionality. These responsibilities include:

1. **Merge Request Management**: Review and merge MRs (merge requests) to the extension ensuring that additions to the codebase align with the project's standards and requirements.
1. **Communication**:
   - Join and particiapte in the `#f_visual_studio_extension` Slack channel to stay informed about updates and discussions related to the extension.
   - _Optionally_, join the `#f_language_server` Slack channel, to stay up-to-date with changes made to the shared language server.

## Code Review Process

As a Maintainer, you will be pinged and assigned Merge Requests to review
and merge. For example:

```plaintext
Hey @john-slaughter! Can you please review and merge this MR? Thanks!

/assign_reviewer @john-slaughter
```

### Evaluation Criteria

When reviewing a Merge Request, please use your best
discretion to ensure that a Merge Request meets our functional
and internal quality criteria. Some questions to ask yourself:

- **Functionality**:

  - Is the change beneficial and desired? Verify against issues referenced in the MR description.
  - Is the code functional? Test thoroughly, including edge cases.
  - Does it maintain the integrity of existing functionalities?
    Conduct a thorough smoke test to ensure basic operability.

- **Maintainability**:
  - Were linting rules disabled in this change? If so ask clarifying questions as to why.
  - Could a simpler solution have been implemented?
  - Evaluate the clarity of the new changes and their impact on the code's readability and structure.
  - Consider the effects on existing code cohesion and responsibilities, as well as any new unwanted code dependencies.
  - Is the code tested? Have we considered edge cases?

**_IMPORTANT_**: As a Maintainer, you do not have to verify everything yourself. You may
choose to delegate to the contributor (or a Domain Expert). For example:

```plaintext
**question:** This looks like it might cause issues when the user is not an admin. Have we tested this scenario? Could you include a screenshot for this please?
```

## Merging

When a Merge Request is ready to merge:

1. Approve the Merge Request.
1. Changelogs are generated based upon commit messages. Check that the commit messages follow the [style guide](https://docs.gitlab.com/ee/development/changelog.html). If the commits do not follow the style guide, you may consider either:
   1. Squash the commits on merge through the GitLab UI. Check the **Squash commits** checkbox and **Modify commit messages** next to the **Merge** button. Modify the squash commit message following the [style guide](https://docs.gitlab.com/ee/development/changelog.html). The merge commit message does not need to follow the style guide.
   1. Ask the Merge Request contributor to squash the commits themselves.
1. Click **Merge**, which should add the Merge Request to the Merge Train.
   You _should not_ need to start a new pipeline.

## References

- [Code Review Values](https://handbook.gitlab.com/handbook/engineering/workflow/reviewer-values/) for how we balance priorities and
  communication during code review.
- [Project members page](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/project_members?with_inherited_permissions=exclude) to find the current list of active maintainers.
