# Development Process

## Developer Environment

_Required Software:_

- Virtual Machine Software
  - Parallels (Paid)
- Windows 10/11 (Paid)
- Visual Studio 2022 Community (Free, or above)
  - Make sure to select Visual Studio Extensions component during installation
- [.NET Framework 4.7.2 Developer Pack](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net472)
- [Git LFS](https://git-lfs.com/) (needed before cloning the repo)

_Recommended Extras:_

- [Cmder console replacement](https://cmder.app/)
  - Comes with `git`
- [LINQPad](https://www.linqpad.net/)

### Testing

Tests will also run as part of the pipeline.

The tests are mostly unit tests, but there are some integration tests, such as
`TextDocumentCompletionAsync` in `LsClientTsTests`. This test calls out to a
live Language Server. As this can fail, the method retries up to 50 times to
prevent false positives.

#### Prerequisites

Before running tests, ensure personal access tokens and environment variables
are configured correctly:

1. [Create a GitLab personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with the `api` and `read_user` scopes.
1. [Create a Windows Server environment variable](https://learn.microsoft.com/en-us/windows-server/administration/windows-commands/set_1) called `GITLAB_TOKEN`. Set it to the value of your personal access token.
1. To give Visual Studio access to the environment variable, restart Visual Studio.

### Install the extension

You can install the extension in a number of ways. Some methods automatically start the install process, but for those that don't, double-click on the file ending in `.vsix` to start the extension installation process:

- **From Visual Studio**: From the main menu, go to Extensions -> Manage Extensions and search for `gitlab`.
- **From the Marketplace**: Go directly to the [extension overview page](https://marketplace.visualstudio.com/items?itemName=GitLab.GitLabExtensionForVisualStudio), or search the Marketplace for `gitlab`.
- **From a local build**: After a successful build, browse to `GitLab.Extension\bin` within the code directory and then browse the `Debug` or `Release` directory, depending on what you targeted for your build, and find the `GitLab.Extension.vsix` file.
- **From a pipeline build**: Download the [job articfacts](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html#download-job-artifacts) for a pipeline. You need the `build:archive` file. After downloading, browse to `GitLab.Extension\bin\Release` and find the `GitLab.Extension.vsix` file.

### Debugging

1. Make sure the GitLab extension is uninstalled prior to starting the first debugging session. On the first debugging session, VS will create a new profile for the debugging session that will include any installed plugins, plus automatically installing the local build of the extension.
1. Once the first debugging session has been started, the extension can be safely installed in the non-debug VS instance for normal usage.
1. The settings are shared between the debug VS instance and the non-debug instance.
1. Never install the GitLab extension from the market place in the debug VS instance.
1. Never uninstall the GitLab extension in the debug VS instance. This will cause VS to stop automatically installing it in debug sessions. Getting it working again is a pain.
1. The extension will not fully start until a supported file is opened.

Set some breakpoints in the `GitLab.Extension` project. The classes and methods represented in [How Visual Studio Proposals Work](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/docs/developer/how-proposals-work.md) and the [architecture diagram](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/docs/developer/architecture.md#diagrams) would be a good place to start. Launch the debugger: `Debug` -> `Start Debugging`.

This will launch another instance of Visual Studio. From here, open a solution. It can be the solution for the extension, or another one.
Type some code as you would regularly to get some code suggestions. The debug log is written to the GitLab extension output window and you can view it through `Output` -> `Show output from Gitlab Extension`. The logging level can be can controlled via settings.

## Troubleshooting

### Environment variable `GITLAB_TOKEN` not found

If you try to run tests without completing [the prerequisites](#prerequisites) for
setting the `GITLAB_TOKEN`:

- All the tests fail immediately.
- The **Output > Tests** window shows this error message:

  ```plaintext
  Setup failed for test fixture GitLab.Extension.Tests.TestData
  System.TypeInitializationException : The type initializer for 'GitLab.Extension.Tests.TestData' threw an exception.
    ----> System.ArgumentException : Required environment variable 'GITLAB_TOKEN' was not found. This is required to run the tests.
  ```

After the environment variable is set, you can run tests locally from the main menu
in Visual Studio in **Tests > Run All Tests**.
