﻿using NUnit.Framework;
using GitLab.Extension.SettingsUtil;
using Serilog;
using Serilog.Events;
using Autofac;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Shell;
using Microsoft.Build.Framework;

namespace GitLab.Extension.Tests
{
    [TestFixture]
    public class LogginTests : TestBase
    {
        private ISettings _settings;

        [SetUp]
        public void Setup()
        {
            CreateBuilder()
                .RegisterLogging()
                .RegisterSettings()
                .RegisterObservables()
                .BuildScope();

            _settings = _scope.Resolve<ISettings>();
        }

        [TearDown]
        public void Teardown()
        {
            TestData.ResetSettings(_settings);
        }

        [Test]
        [ExecuteOnMainThread]
        public void ChangeLogLevelSetting()
        {
            var sink = new TestLoggingSink(null);
            Logging.TestingOnlyConfigureLogging(sink);
            Logging.MinimumLevel = LogEventLevel.Warning;
            sink.LogMessages.Clear();

            Assert.AreEqual(0, sink.LogMessages.Count, 
                "Expected 0 log messages in the sink.");

            Log.Debug("Hi this is a debug message");
            Assert.AreEqual(0, sink.LogMessages.Count,
                "Expected 0 log messages in the sink.");

            Log.Warning("Hi this is a warning message");
            Assert.AreEqual(1, sink.LogMessages.Count,
                "Expected a 1 log message in the sink.");

            sink.LogMessages.Clear();
            Logging.MinimumLevel = LogEventLevel.Debug;

            Assert.AreEqual(0, sink.LogMessages.Count,
                "Expected a single log message in the sink.");

            Log.Debug("Hi this is a debug message");
            Assert.AreEqual(1, sink.LogMessages.Count,
                "Expected a 1 log message in the sink.");

            Log.Warning("Hi this is a warning message");
            Assert.AreEqual(2, sink.LogMessages.Count,
                "Expected a 2 log message in the sink.");
        }
    }
}
