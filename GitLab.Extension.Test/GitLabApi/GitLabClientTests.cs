﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.GitLabApi;
using GitLab.Extension.GitLabApi.Model;
using GitLab.Extension.Utility.Results;
using Moq;
using Moq.Protected;
using NUnit.Framework;

namespace GitLab.Extension.Tests.GitLabApi
{
    [TestFixture]
    [SuppressMessage("Style", "VSTHRD200:Use \"Async\" suffix for async methods")]
    public class GitLabClientTests
    {
        [SetUp]
        public void SetUp()
        {
            _mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            _httpClient = new HttpClient(_mockHttpMessageHandler.Object);
            _httpClient.BaseAddress = new Uri("http://example.com/");
            _gitLabClient = new GitLabClient(_httpClient);
        }

        private Mock<HttpMessageHandler> _mockHttpMessageHandler;
        private HttpClient _httpClient;
        private GitLabClient _gitLabClient;

        [Test]
        public async Task GetMetadataAsync_Success_ReturnsMetadata()
        {
            // Arrange
            var expectedMetadata = new Metadata { Version = "1.0.0" };
            var expectedMetadataJson =
                JsonSerializer.Serialize(
                    expectedMetadata,
                    Extension.GitLabApi.Constants.GitLabJsonSerializerOptions);

            _mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(expectedMetadataJson)
                });

            // Act
            var result = await _gitLabClient.GetMetadataAsync();

            // Assert
            Assert.IsNotNull(result);

            var metadata = result.GetValueOrDefault();
            Assert.AreEqual(expectedMetadata.Version, metadata.Version);
        }

        [Test]
        public async Task IsCodeSuggestionsEnabledForProjectAsync_StatusCodeOK_ReturnsTrue()
        {
            // Arrange
            _mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK
                });

            var requestModel = new CodeSuggestionsEnabledRequest();

            // Act
            var result = await _gitLabClient.IsCodeSuggestionsEnabledForProjectAsync(requestModel);

            // Assert
            Assert.IsTrue(result.GetValueOrDefault());
        }

        [Test]
        public async Task IsCodeSuggestionsEnabledForProjectAsync_StatusCodeNotOK_ReturnsFalse()
        {
            // Arrange
            _mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.Accepted
                });

            var requestModel = new CodeSuggestionsEnabledRequest();

            // Act
            var result = await _gitLabClient.IsCodeSuggestionsEnabledForProjectAsync(requestModel);

            // Assert
            Assert.IsFalse(result.GetValueOrDefault());
        }

        [Test]
        public async Task IsCodeSuggestionsEnabledForUserAsync_CodeSuggestionsEnabled_ReturnsTrue()
        {
            // Arrange
            var jsonResponse = @"{""data"": {""currentUser"": {""duoCodeSuggestionsAvailable"": true}}}";
            _mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(jsonResponse, Encoding.UTF8, "application/json")
                });

            // Act
            var result = await _gitLabClient.IsCodeSuggestionsEnabledForUserAsync();

            // Assert
            Assert.IsTrue(result.GetValueOrDefault());
        }

        [Test]
        public async Task IsCodeSuggestionsEnabledForUserAsync_CodeSuggestionsDisabled_ReturnsFalse()
        {
            // Arrange
            var jsonResponse = @"{""data"": {""currentUser"": {""duoCodeSuggestionsAvailable"": false}}}";
            _mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(jsonResponse, Encoding.UTF8, "application/json")
                });

            // Act
            var result = await _gitLabClient.IsCodeSuggestionsEnabledForUserAsync();

            // Assert
            Assert.False(result.GetValueOrDefault());
        }

        [Test]
        public async Task IsCodeSuggestionsEnabledForUserAsync_DuoCodeSuggestionsAvailableFieldMissing_ReturnsFalse()
        {
            // Arrange
            var jsonResponse =
                @"{""errors"":[{""message"":""Field 'duoCodeSuggestionsAvailable' doesn't exist on type 'CurrentUser'"",""locations"":[{""line"":3,""column"":5}],""path"":[""query aiAccess"",""currentUser"",""duoCodeSuggestionsAvailable""],""extensions"":{""code"":""undefinedField"",""typeName"":""CurrentUser"",""fieldName"":""duoCodeSuggestionsAvailable""}}]}";
            _mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(jsonResponse, Encoding.UTF8, "application/json")
                });

            // Act
            var result = await _gitLabClient.IsCodeSuggestionsEnabledForUserAsync();

            // Assert
            Assert.False(result.GetValueOrDefault());
        }
    }
}