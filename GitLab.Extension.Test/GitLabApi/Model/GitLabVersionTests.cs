﻿using System;
using GitLab.Extension.GitLabApi.Model;
using NUnit.Framework;

namespace GitLab.Extension.Tests.GitLabApi.Model
{
    [TestFixture]
    public class GitLabVersionTests
    {
        [Test]
        public void Parse_ValidVersionString_ReturnsCorrectVersionObject()
        {
            // Arrange
            const string versionStr = "1.2.3-alpha";

            // Act
            var version = GitLabVersion.Parse(versionStr);

            // Assert
            Assert.AreEqual(1u, version.Major);
            Assert.AreEqual(2u, version.Minor);
            Assert.AreEqual(3u, version.Patch);
            Assert.AreEqual("alpha", version.PreRelease);
        }

        [TestCase("1.2", typeof(ArgumentException))]
        [TestCase("not_a_version", typeof(ArgumentException))]
        [TestCase(null, typeof(ArgumentNullException))]
        public void Parse_InvalidVersionString_ThrowsArgumentException(
            string versionStr,
            Type expectedExceptionType)
        {
            // Act & Assert
            Assert.Throws(expectedExceptionType, () => GitLabVersion.Parse(versionStr));
        }

        [Test]
        public void CompareTo_CorrectlyComparesVersions()
        {
            // Arrange
            var lowerVersion = new GitLabVersion(1);
            var higherVersion = new GitLabVersion(2);

            // Act & Assert
            Assert.IsTrue(lowerVersion.CompareTo(higherVersion) < 0);
            Assert.IsTrue(higherVersion.CompareTo(lowerVersion) > 0);
        }

        [Test]
        public void Equals_EqualVersions_ReturnsTrue()
        {
            // Arrange
            var version1 = new GitLabVersion(1, 0, 0, "beta");
            var version2 = new GitLabVersion(1, 0, 0, "beta");

            // Act & Assert
            Assert.IsTrue(version1.Equals(version2));
        }

        [Test]
        public void Equals_DifferentVersions_ReturnsFalse()
        {
            // Arrange
            var version1 = new GitLabVersion(1);
            var version2 = new GitLabVersion(2);

            // Act & Assert
            Assert.IsFalse(version1.Equals(version2));
        }

        [Test]
        public void ToString_ReturnsCorrectFormat()
        {
            // Arrange
            var version = new GitLabVersion(1, 2, 3, "rc1");

            // Act
            var versionStr = version.ToString();

            // Assert
            Assert.AreEqual("1.2.3-rc1", versionStr);
        }

        [Test]
        public void OperatorOverloads_CanPerformEqualityComparisons()
        {
            // Arrange
            var v1 = new GitLabVersion(1);
            var v2 = new GitLabVersion(2);
            var v1Clone = new GitLabVersion(1);

            // Act & Assert
            Assert.IsTrue(v1 < v2);
            Assert.IsTrue(v2 > v1);
            Assert.IsTrue(v1 <= v1Clone);
            Assert.IsTrue(v1 >= v1Clone);
            Assert.IsFalse(v1 > v2);
            Assert.IsFalse(v2 < v1);
        }

        [Test]
        public void ImplicitConversion_FromString_ParsesCorrectly()
        {
            // Arrange
            var versionStr = "3.2.1-beta";

            // Act
            GitLabVersion version = versionStr;

            // Assert
            Assert.AreEqual(3u, version.Major);
            Assert.AreEqual(2u, version.Minor);
            Assert.AreEqual(1u, version.Patch);
            Assert.AreEqual("beta", version.PreRelease);
        }
    }
}