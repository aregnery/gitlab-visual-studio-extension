﻿using System;
using System.Linq;
using GitLab.Extension.GitLabApi;
using GitLab.Extension.GitLabApi.Errors;
using NUnit.Framework;

namespace GitLab.Extension.Tests.GitLabApi
{
    [TestFixture]
    public class GitLabApiSettingsTests
    {
        private const string ValidAccessToken = "valid_access_token";
        private const string ValidBaseUrl = "https://gitlab.com";

        [Test]
        public void Create_WithValidParameters_ReturnsSuccessResult()
        {
            // Act
            var result = GitLabApiSettings.Create(ValidBaseUrl, ValidAccessToken);

            // Assert
            Assert.IsTrue(result.IsSuccess(out var settings));
            Assert.AreEqual(new Uri(ValidBaseUrl), settings.BaseUri);
            Assert.AreEqual(ValidAccessToken, settings.AccessToken);
        }

        [TestCase(null)]
        [TestCase("")]
        public void Create_WithInvalidAccessToken_ReturnsInvalidAccessToken(
            string accessToken)
        {
            // Act
            var result = GitLabApiSettings.Create(ValidBaseUrl, accessToken);

            // Assert
            Assert.IsTrue(result.IsError(out var error));
            Assert.IsInstanceOf(typeof(GitLabApiConfigurationError), error);

            var firstError = error.Reasons.FirstOrDefault();
            Assert.NotNull(firstError);
            Assert.IsInstanceOf(typeof(InvalidAccessToken), firstError);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase("/api")]
        [TestCase("not_a_valid_uri")]
        public void Create_WithInvalidBaseUrl_ReturnsInvalidBaseUrlResult(
            string baseUrl)
        {
            // Act
            var result = GitLabApiSettings.Create(baseUrl, ValidAccessToken);

            // Assert
            
            Assert.IsTrue(result.IsError(out var error));
            Assert.IsInstanceOf(typeof(GitLabApiConfigurationError), error);

            var firstError = error.Reasons.FirstOrDefault();
            Assert.NotNull(firstError);
            Assert.IsInstanceOf(typeof(InvalidBaseUrl), firstError);
        }
    }
}