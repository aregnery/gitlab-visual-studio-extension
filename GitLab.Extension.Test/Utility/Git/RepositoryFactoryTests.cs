using System;
using System.IO;
using GitLab.Extension.Utility.Git;
using GitLab.Extension.Utility.Results.Errors;
using NUnit.Framework;

namespace GitLab.Extension.Tests.Utility.Git
{
    [TestFixture]
    public class RepositoryFactoryTests
    {
        [Test]
        public void GetRepository_WhenPathIsNull_ReturnsArgumentExceptionError()
        {
            // Arrange
            var factory = new RepositoryFactory();

            // Act
            var result = factory.GetRepository(null);
            
            // Assert
            Assert.That(result.IsError(out var error), Is.True);
            Assert.That(error, Is.InstanceOf<ExceptionalError>());
            Assert.That((error as ExceptionalError)?.Exception, Is.InstanceOf<ArgumentException>());
        }

        [Test]
        public void GitRepository_WhenDirectoryDoesNotExist_ReturnsDirectoryNotFoundExceptionError()
        {
            // Arrange
            const string path = "non/existent/path";
            var factory = new RepositoryFactory();
            
            // Act
            var result = factory.GetRepository(path);
            
            // Assert
            Assert.That(result.IsError(out var error), Is.True);
            Assert.That(error, Is.InstanceOf<ExceptionalError>());
            Assert.That((error as ExceptionalError)?.Exception, Is.InstanceOf<DirectoryNotFoundException>());
        }
    }
}