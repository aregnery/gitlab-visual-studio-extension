using System;
using System.Reactive.Linq;
using GitLab.Extension.CodeSuggestions.State.Validators;
using GitLab.Extension.SettingsUtil;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.CodeSuggestions.State.Enabled.Validators
{
    [TestFixture]
    public class SettingsGitLabUriValidatorTests
    {
        [SetUp]
        public void SetUp()
        {
            _settingsMock = new Mock<ISettings>();
        }

        private Mock<ISettings> _settingsMock;

        [Test]
        public void ValidationResultObservable_WithValidUri_EmitsEnabledState()
        {
            // Arrange
            const string validUri = "https://gitlab.com";

            _settingsMock
                .Setup(s => s.GitLabUrl)
                .Returns(validUri);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new SettingsGitLabUriValidator(settingsObservable);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state => { Assert.IsTrue(state.IsCodeSuggestionsEnabled); });
        }

        [Test]
        public void ValidationResultObservable_WithInvalidUri_EmitsDisabledState()
        {
            // Arrange
            const string invalidUri = "gitlab.com"; // Missing scheme (http/https)

            _settingsMock
                .Setup(s => s.GitLabUrl)
                .Returns(invalidUri);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new SettingsGitLabUriValidator(settingsObservable);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
                Assert.AreEqual(
                    Extension.CodeSuggestions.State.Constants.Messages.InvalidConfiguredUrl,
                    state.DisabledMessage);
            });
        }
    }
}
