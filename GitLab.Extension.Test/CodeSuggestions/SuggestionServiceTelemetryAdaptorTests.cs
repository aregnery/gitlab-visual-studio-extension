using System.Collections.Generic;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.Workspace;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Language.Suggestions;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Moq;
using NUnit.Framework;
#pragma warning disable CS0618 // Type or member is obsolete

namespace GitLab.Extension.Tests.CodeSuggestions
{
    [TestFixture]
    public class SuggestionServiceTelemetryAdaptorTests
    {
        private Mock<SuggestionServiceBase> _mockSuggestionServiceBase;
        private Mock<IGitLabCodeSuggestionTelemetryController> _mockTelemetryController;
        private Mock<ITextView> _mockTextView;
        
        // ReSharper disable once NotAccessedField.Local
        // we only need to keep this reference around so that the GC doesn't collect
        private SuggestionServiceTelemetryAdaptor _adaptor;

        private static readonly GitLabProposal TestProposal =
            new GitLabProposal(
                new GitLabProposalMetadata(
                    new WorkspaceId("SolutionName", "path/to/solution"),
                    "test-telemetry-id"),
                null,
                new List<ProposedEdit>(),
                new VirtualSnapshotPoint());

        [SetUp]
        public void SetUp()
        {
            _mockSuggestionServiceBase = new Mock<SuggestionServiceBase>();
            _mockTelemetryController = new Mock<IGitLabCodeSuggestionTelemetryController>();
            _mockTextView = new Mock<ITextView>();
            _adaptor = new SuggestionServiceTelemetryAdaptor(_mockSuggestionServiceBase.Object, _mockTelemetryController.Object);
        }
        
        [Test]
        public void ShouldInvokeOnShownAsync_WhenProposalDisplayedEventIsRaised()
        {
            var args = 
                new ProposalDisplayedEventArgs(
                    "IntelliCodeLineCompletions", 
                    _mockTextView.Object, 
                    TestProposal);

            _mockSuggestionServiceBase.Raise(service => service.ProposalDisplayed += null, this, args);

            _mockTelemetryController.Verify(controller => controller.OnShownAsync(TestProposal.GitLabProposalMetadata), Times.Once);
        }
        
        [Test]
        public void ShouldInvokeOnRejectedAsync_WhenProposalRejectedEventIsRaised()
        {
            var args = 
                new ProposalRejectedEventArgs(
                    "IntelliCodeLineCompletions",
                    _mockTextView.Object, 
                    TestProposal, 
                    TestProposal, 
                    ReasonForUpdate.Updated);

            _mockSuggestionServiceBase.Raise(service => service.ProposalRejected += null, this, args);

            _mockTelemetryController.Verify(controller => controller.OnRejectedAsync(TestProposal.GitLabProposalMetadata), Times.Once);
        }
        
        [Test]
        public void ShouldInvokeOnAcceptedAsync_WhenSuggestionAcceptedEventIsRaised()
        {
            var args = 
                new SuggestionAcceptedEventArgs(
                    "IntelliCodeLineCompletions", 
                    _mockTextView.Object, 
                    TestProposal,
                    TestProposal,
                    ReasonForAccept.AcceptedByCommand);

            _mockSuggestionServiceBase.Raise(service => service.SuggestionAccepted += null, this, args);

            _mockTelemetryController.Verify(controller => controller.OnAcceptedAsync(TestProposal.GitLabProposalMetadata), Times.Once);
        }
        
        [Test]
        public void ShouldNotInvokeTelemetryController_WhenUnknownProviderNameIsProvided()
        {
            var args = 
                new ProposalDisplayedEventArgs(
                    "UnknownProvider", 
                    _mockTextView.Object, 
                    TestProposal);

            _mockSuggestionServiceBase.Raise(service => service.ProposalDisplayed += null, this, args);

            _mockTelemetryController.Verify(controller => controller.OnShownAsync(It.IsAny<GitLabProposalMetadata>()), Times.Never);
        }
    }
}
