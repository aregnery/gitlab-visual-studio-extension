<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "bug" label:

- https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues?label_name%5B%5D=type%3A%3Abug

and verify the issue you're about to submit isn't a duplicate.
--->

### Checklist

<!-- Please test the latest versions, that will remove the possibility that you see a bug that is fixed in a newer version. -->

- [ ] I'm using the latest version of the extension ([see the latest version in the right column of this page](https://marketplace.visualstudio.com/items?itemName=GitLab.GitLabExtensionForVisualStudio))
  - Extension version: _Put your extension version here_
- [ ] I'm using the latest Visual Studio 2022 version ([find the latest version here](https://learn.microsoft.com/en-us/visualstudio/releases/2022/release-notes))
  - Visual Studio 2022 version: _Put your Visual Studio 2022 version here_
- [ ] I'm using a supported version of GitLab ([see README for the supported version](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/README.md#minimum-supported-version))
  - GitLab version: _Put your GitLab version here, or say "happens on `gitlab.com`"_

### Summary

<!-- Summarize the bug encountered concisely -->

### Steps to reproduce

<!-- How one can reproduce the issue - this is very important -->

### What is the current _bug_ behavior?

<!-- What actually happens -->

### What is the expected _correct_ behavior?

<!-- What you should see instead -->

### Relevant logs and/or screenshots

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->

/label ~"type::bug" ~"devops::create" ~"group::editor extensions" ~"Category:Editor Extension"
