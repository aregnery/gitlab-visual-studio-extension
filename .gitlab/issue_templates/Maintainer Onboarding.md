<!--
  Update the title of this issue to: Maintainer Onboarding - [Full Name]
-->

## Basic setup

- [ ] Create a merge request to update [your team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md), adding yourself as a [`trainee_maintainer`](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#learning-to-be-a-maintainer) of the `gitlab-visual-studio-extension` project.
- [ ] Join the `#f_visual_studio_extension` internal Slack channel to connect with your team and stay updated on ongoing discussions.
- [ ] Review the [maintainer responsibilities](../../docs/developer/maintainer_responsibility.md) document to understand your duties and expectations.
- [ ] Familiarize yourself with the project by reading the [development process](../../docs/developer/development-process.md) and [architecture](../../docs/developer/architecture.md).
- [ ] Contribute to the project's documentation or improve this Maintainer Onboarding template through a merge request.
- [ ] _Optional_: Schedule a project walkthrough with an existing maintainer to get familiar with the codebase.
- [ ] _Optional_: Schedule a [code review pairing](#code-review-pairing) with an existing maintainer to learn the review process firsthand.
- [ ] _Optional_: Read through the [code review page in the handbook](https://about.gitlab.com/handbook/engineering/workflow/code-review/) and [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
- [ ] _Optional_: Learn about the pathway [to becoming a maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer).

### Code review pairing

Much like pair programming, pairing on code review is a great way to share knowledge and collaborate on merge requests. This activity is great for trainee maintainers to participate with maintainers, and learn their process of code review.

A **private code review session** (unrecorded) involves one primary reviewer, and a shadow. If more than one shadow wishes to observe a private session, please consider obtaining consent from the merge request author.

A **public code review session** (recorded) involves a primary reviewer and one or more shadows. The recording is released publicly, such as to GitLab Unfiltered.

- If the merge request author is a GitLab team member, please consider obtaining consent from them.
- If the merge request author is a community contributor, you **must** obtain consent from them.
- Do **not** release reviews of security merge requests publicly.

## Transition to official maintainer

After you are handling MRs effectively, and your reviews meet the maintainer criteria:

- [ ] Create a merge request to update [your team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md), proposing yourself as a maintainer for the `gitlab-visual-studio-extension` project. Assign it to your manager.
- [ ] Ask an existing maintainer to give you the maintainer role in this GitLab project.
- [ ] Keep reviewing, start merging :metal:

Even when you are a maintainer, you can still request help from other maintainers if you get a merge request that is too complex for you, or requires a second opinion.

Need help? Ask in the `#g_editor-extensions` or `#f_visual_studio_extension` Slack channels.
