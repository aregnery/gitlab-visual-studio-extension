# Contributing to the GitLab Extension for Visual Studio

## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

All Documentation content that resides under the [docs/ directory](/docs) of this
repository is licensed under Creative Commons:
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

_This notice should stay as the first item in the CONTRIBUTING.md file._

---

Thank you for your interest in contributing to the GitLab Extension for Visual Studio! This guide details how to contribute
to this extension in a way that is easy for everyone. These are mostly guidelines, not rules.
Use your best judgement, and feel free to propose changes to this document in a merge request.

## Code of Conduct

We want to create a welcoming environment for everyone who is interested in contributing. Visit our [Code of Conduct page](https://about.gitlab.com/community/contribute/code-of-conduct/) to learn more about our commitment to an open and welcoming environment.

## Getting Started

### Reporting Issues

Create a [new issue from the "Bug" template](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues/new?issuable_template=Bug) and follow the instructions in the template.

### Proposing Features

Create a [new issue from the "Feature Proposal" template](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues/new?issuable_template=Feature%20Proposal) and follow the instructions in the template.

### Your First Code Contribution?

Read about the extension architecture in [the developer docs](docs/developer/architecture.md). This document explains how we structure our code and will help you orientate yourself in the codebase.

For newcomers to the project, you can take a look at issues labeled as `Accepting merge requests`
as available [here](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues?label_name[]=Accepting%20merge%20requests).

### Configuring Development Environment

The following instructions will help you run the GitLab Visual Studio Extension locally.

#### Step - 1 : Installation Prerequisites

We're assuming that you already have [Visual Studio 2022](https://visualstudio.microsoft.com/downloads/) installed along
with the [GitLab](https://marketplace.visualstudio.com/items?itemName=GitLab.GitLabExtensionForVisualStudio) Extension for Visual Studio installed
and configured, if not, do that first! If already done, proceed ahead.

#### Step - 2 : Fork and Clone

- Use your GitLab account to [fork](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio/-/forks/new) this project
  - Don't know how forking works? Refer to [this guide](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html#doc-nav).
  - Don't have a GitLab account? [Create one](https://gitlab.com/users/sign_up)! It is free and it is awesome!
- Visit your forked project (usually URL is `https://gitlab.com/<your user name>/editor-extensions/gitlab-visual-studio`).
- Set up pull mirroring to keep your fork up to date.
  - [How do I mirror repositories?](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pulling-from-a-remote-repository)
  - Use `https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio.git` as the **Git repository URL**.
- Go to your project overview and copy the SSH or HTTPS URL to clone the project into your system.
  - Don't know how to clone a project? Refer to [this guide](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html#clone-your-project).

#### Step - 3 : Run tests

[Run unit tests with Test Explorer](https://learn.microsoft.com/en-us/visualstudio/test/run-unit-tests-with-test-explorer?view=vs-2022)

#### Step - 4 : Add documentation

If you added or changed a feature, add the documentation to the README.

The majority of the user documentation is directly in [`README.md`](README.md), because that file is rendered in:

- The [extension marketplace page](https://marketplace.visualstudio.com/items?itemName=GitLab.GitLabExtensionForVisualStudio).
- The extension overview directly in Visual Studio.

To add documentation that includes a new image:

1. Add images into the `docs/assets` folder, and commit the changes.
1. Edit the README file, and insert full permalinks to your new images.
   The permalinks contain the commit SHA from your first commit, and are
   in the form of:

   ```plaintext
   https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extensio/-/raw/<COMMIT_SHA>/docs/assets/imagename.png
   ```

1. Commit your text changes.

For more examples, refer to the `gif` images in the README file.

### Adding new icons

If you're adding new icons to extension, you will need to put svg file named exactly with the name of new icon in `src/assets/icons`.
Please use `./assets/gitlab_icons.woff` as `fontPath` for icon in `package.json`. This font will be auto-generated

For example, consider you're adding new icon to `contributes.icon` section of `package.json`:

```json
{
  "gitlab-new-icon": {
    "description": "GitLab New Icon",
    "default": {
      "fontPath": "./assets/gitlab_icons.woff",
      "fontCharacter": "\\eA05"
    }
  }
}
```

In that case you need to place icon SVG at `src/assts/icons/gitlab-new-icon.svg`.

Build process will fail if one of the referenced icons are missing.

### Opening Merge Requests

Steps to opening a merge request to contribute code to the GitLab Extension for Visual Studio is similar to any other open source project.
You develop in a separate branch of your own fork and the merge request should have a related issue open in the project.
Any Merge Request you wish to open in order to contribute to the GitLab Extension for Visual Studio, be sure you have followed through the steps from [Configuring Development Environment](#configuring-development-environment).

In this project, we don't [close issues automatically when the MR gets merged](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically). Instead, we close the issues when the MR change is [released](docs/developer/release-process.md). Please replace `Closes #<issueId>` in the MR description with `Relates to #<issueId>`.
