# Changelog

## 0.37.0 (2024-04-16)

### other (1 change)

- [Add additional debug logging to GitlabProposalSource.cs](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e0dbd903c6b8ac191674c6295f3c9ee07a7f41b2) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!132))

## 0.36.0 (2024-04-16)

### changed (1 change)

- [Update Language Server to v4.3.0](gitlab-org/editor-extensions/gitlab-visual-studio-extension@0948a8f2a145dc7f752cfea05593c531771e7b9b) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!131))

## 0.35.0 (2024-04-09)

### changed (2 changes)

- [Add observable validators for determining the state of code suggestions](gitlab-org/editor-extensions/gitlab-visual-studio-extension@6a4e765729fee5affefa06735d1da2b6a1df132b) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!130))
- [Introduce workspace lifecycle management](gitlab-org/editor-extensions/gitlab-visual-studio-extension@ede378541ff66656cfd059a295f9c78cf8e2b998) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!122))

### added (1 change)

- [Add additional supported languages for code suggestions](gitlab-org/editor-extensions/gitlab-visual-studio-extension@48e8cf3c0712af4de8ab32f6b0c66ff98848ea7d) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!128))

## 0.34.0 (2024-02-15)

### added (2 changes)

- [Introduce observable state for code suggestions](gitlab-org/editor-extensions/gitlab-visual-studio-extension@b7a23147610beff4a0f0fab189cc7253b9602c6b) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!115))
- [Introduce Result utility type for improved control flow](gitlab-org/editor-extensions/gitlab-visual-studio-extension@2aedb365cefc37bf146e79509454d5996a462ba8) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!113))

### changed (2 changes)

- [feat: Introduce client to interface with GitLab API](gitlab-org/editor-extensions/gitlab-visual-studio-extension@5d15734eb7e8ad0ff4813fc0f9189aa72deaac09) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!114))
- [Update Autofac Dependency](gitlab-org/editor-extensions/gitlab-visual-studio-extension@8e36e923c1a4116b38e4f1f668eca4e943399991) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!112))

## 0.33.0 (2024-01-29)

No changes.

## 0.32.0 (2023-12-14)

### fixed (1 change)

- [Fix formatting on commit](gitlab-org/editor-extensions/gitlab-visual-studio-extension@f832f14620b36302d6d63d72be65a38d7c791992) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!100))

### changed (1 change)

- [Update Language Server to v3.24.1](gitlab-org/editor-extensions/gitlab-visual-studio-extension@5ec2b72d93696d743691a8be3c0310c0e38cdc7f) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!101))

### added (1 change)

- [Use SuggestionServiceBase to accurately track telemetry events](gitlab-org/editor-extensions/gitlab-visual-studio-extension@2f0cb733a2cb410c8fb1a04bdea156f2817c8a75) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!92))

### other (1 change)

- [Create Language Server Upgrade Developer Documentation](gitlab-org/editor-extensions/gitlab-visual-studio-extension@225c9c88cfdda7c9516ae77584bc1b652bd3df5e) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!97))

## 0.31.0 (2023-12-06)

### changed (2 changes)

- [Upgrade extension to use language server v3.20.2](gitlab-org/editor-extensions/gitlab-visual-studio-extension@39d0729c8448a961263f773cfc5c96ad1b58d9f5) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!98))
- [Update language server to v3.16.1](gitlab-org/editor-extensions/gitlab-visual-studio-extension@15f81e7131ac7c295cdc0b114955a28ace3a4e1b) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!94))

### other (1 change)

- [Add code suggestion telemetry manual testing plan to developer docs](gitlab-org/editor-extensions/gitlab-visual-studio-extension@3dacd4d862eed9464541b7063c29fa35d67940d4) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!96))

### added (1 change)

- [Introduce workspaces with WorkspaceId](gitlab-org/editor-extensions/gitlab-visual-studio-extension@5ecb9751fec19a59012198320d11a2ec8c92c5ed) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!88))

## 0.30.0 (2023-11-07)

### changed (2 changes)

- [Upgrade Language Server to v3.12.1](gitlab-org/editor-extensions/gitlab-visual-studio-extension@f11443999b6d67c79b046b8b8cbf7e6fbd781f40) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!90))
- [upgrade language server to v3.6.0](gitlab-org/editor-extensions/gitlab-visual-studio-extension@94fff7e2c153d9ca8b1564e40b5076882dcab8d3) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!83))

### fixed (1 change)

- [StatusBar check initialization status before handling settings change](gitlab-org/editor-extensions/gitlab-visual-studio-extension@b9593df0713ee8ef62f68db2225c1b098ffeb179) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!89))

## 0.29.0 (2023-10-20)

### added (1 change)

- [Add custom command with configurable shortcut for toggling on/off code suggestions](gitlab-org/editor-extensions/gitlab-visual-studio-extension@9b704e1de1b145e2174d17daa0ec5f805aba9362) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!80))

## 0.28.0 (2023-10-19)

### changed (1 change)

- [Always initialize the extension UX](gitlab-org/editor-extensions/gitlab-visual-studio-extension@385733608798dd5ab2c595e064cf38fe76371976) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!78))

### added (1 change)

- [Added notification when access token is invalid](gitlab-org/editor-extensions/gitlab-visual-studio-extension@443b6f8bac6ddfc79e8dae7593a9557d5f6db280) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!77))

## 0.27.0 (2023-10-16)

### changed (1 change)

- [Improve detection of C/C++ files using extension](gitlab-org/editor-extensions/gitlab-visual-studio-extension@afc7c6d553dc5190daffefa323574ff3a233cf21) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!79))

### fixed (1 change)

- [Fix null reference error when language not detected](gitlab-org/editor-extensions/gitlab-visual-studio-extension@25f99d229777d8aebd3e4ca6044c26d3af023873) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!79))

### added (1 change)

- [Added setting to allow users to enable/disable sending telemetry](gitlab-org/editor-extensions/gitlab-visual-studio-extension@50ae637354d488507c96195729bfeec8825af857) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!75))

## 0.26.0 (2023-10-03)

### changed (1 change)

- [Upgrade to Language Server v3.3.0](gitlab-org/editor-extensions/gitlab-visual-studio-extension@0782a2e80471fc0b425cfb26f90e0f89f24d20e9) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!76))

## 0.25.0 (2023-09-29)

### fixed (1 change)

- [Prevent extension disconnect on telemetry error](gitlab-org/editor-extensions/gitlab-visual-studio-extension@64ed6dfd5bd4ccbdb12b4e697bb68a8311272658) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!74))

## 0.24.0 (2023-09-28)

### changed (1 change)

- [Upgrade to language server v3.0](gitlab-org/editor-extensions/gitlab-visual-studio-extension@dd8da92a07dde7e503a4fc677a47a04e30621267) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!73))

## 0.23.0 (2023-09-26)

No changes.

## 0.22.0 (2023-09-21)

### changed (1 change)

- [Update version in AssemblyInfo.cs during CI build](gitlab-org/editor-extensions/gitlab-visual-studio-extension@07bb04c1589bd493b069872c9a3c376744f3a95e) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!71))

## 0.21.0 (2023-09-20)

### fixed (1 change)

- [Fix bug preventing ide_name from being populated](gitlab-org/editor-extensions/gitlab-visual-studio-extension@ccf964f42579e753beaeee75e8eb6422725639d9) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!70))

## 0.20.0 (2023-09-19)

### fixed (1 change)

- [Fix ide_version field serialization in init message](gitlab-org/editor-extensions/gitlab-visual-studio-extension@cb9c6bfa514ea1ceb1dc4f98b2a041dfffd5d133) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!69))

## 0.19.0 (2023-09-15)

### changed (1 change)

- [Get code suggestions from comments and strings](gitlab-org/editor-extensions/gitlab-visual-studio-extension@f450df9ec99ebb2fbae8ded03c62c9927e780af6) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!68))

## 0.18.0 (2023-09-15)

### changed (1 change)

- [Telemetry, send additional messages](gitlab-org/editor-extensions/gitlab-visual-studio-extension@61dd4123495a852e8d5b58d148833e160a2a7bc7) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!66))

### added (1 change)

- [Send code suggestion telemetry to language server](gitlab-org/editor-extensions/gitlab-visual-studio-extension@7afb30a05eaeccdfd4dcf0d4addded3123814358) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!60))

## 0.17.0 (2023-09-13)

### changed (1 change)

- [Set languageId field for textDocument/didOpen message](gitlab-org/editor-extensions/gitlab-visual-studio-extension@8dfb21b83a6e37a1b2b5e5cfc648ad3e570a371f) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!65))

## 0.16.0 (2023-09-12)

### changed (1 change)

- [Display status icon and output window earlier](gitlab-org/editor-extensions/gitlab-visual-studio-extension@a08ea9494863d86c28c17df7b540e0b0e89040f2) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!64))

## 0.15.0 (2023-08-29)

No changes.

## 0.14.0 (2023-08-22)

### changed (2 changes)

- [Switch to TypeScript language server](gitlab-org/editor-extensions/gitlab-visual-studio-extension@69d422cd299e52b5f43f8fa3ed7821107121f4b9) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!52))
- [Upgrade to TypeScript Lang Server v0.0.7](gitlab-org/editor-extensions/gitlab-visual-studio-extension@67be3655665cfebf8c8250966e783c9ba9531909) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!50))

## 0.13.0 (2023-08-21)

No changes.

## 0.12.0 (2023-08-19)

No changes.

## 0.11.0 (2023-08-19)

### added (1 change)

- [Support code suggestions for additional file types](gitlab-org/editor-extensions/gitlab-visual-studio-extension@a7bc443e4171a799142f3cfabb2807f7ed5badd8) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!45))

## 0.10.0 (2023-07-12)

### added (2 changes)

- [Limit suggestions to supported languages](gitlab-org/editor-extensions/gitlab-visual-studio-extension@5bd6e716b6d0a8db9172acf6b69b677716939acb) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!34))
- [Add /docs directory](gitlab-org/editor-extensions/gitlab-visual-studio-extension@4351daf4449de30fde1e1f325d83fd4ae0615afc) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!20))

### fixed (1 change)

- [Better handle empty suggestion](gitlab-org/editor-extensions/gitlab-visual-studio-extension@0a552b8599635b509ed05f58705b748fa9658edb) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!33))

## 0.9.0 (2023-07-05)

### added (1 change)

- [Control logging level through a user setting](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e877a618d05fa4df5b7e12c88a667b2654c0f0d6) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!31))

## 0.8.0 (2023-06-30)

### added (1 change)

- [Adding logging and ability to enable writing log to disk](gitlab-org/editor-extensions/gitlab-visual-studio-extension@2d8dd7756f057bd61de51e8b75a203af3d5c71cd) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!24))

## 0.7.0 (2023-06-28)

### fixed (1 change)

- [Show correct status icon when extension is loaded](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e5d2ac2c6e89e191e7b91a1a4c915e80aa44eae1) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!29))

## 0.6.0 (2023-06-28)

### added (1 change)

- [Make extension public, also tweak README.md](gitlab-org/editor-extensions/gitlab-visual-studio-extension@125e7212b1e96f378ccccb9bd7a3cbc20a68f363) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!28))

## 0.5.0 (2023-06-28)

### fixed (1 change)

- [Pull latest code before building](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e7f74927aca476546fb94b1a6f015949fddd1c0b) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!27))

## 0.4.0 (2023-06-28)

### fixed (1 change)

- [Add a `git pull` before publishing VSIX](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e55a1fb84f312531340affd7144a85080d828503) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!26))

## 0.3.0 (2023-06-28)

### added (1 change)

- [Integrate latest language server iteration](gitlab-org/editor-extensions/gitlab-visual-studio-extension@765f58296c6ff2ddf81601031c219be2e751d9ba) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!21))

### changed (1 change)

- [Re-arrange documentation](gitlab-org/editor-extensions/gitlab-visual-studio-extension@a74794530c87534e921d8ec50d2b53391596f96b) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!19))

## 0.2.0 (2023-06-22)

No changes.

## 0.1.0 (2023-06-22)

### added (1 change)

- [Add output/buildtag file](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e3ad6661962023498db90b7340f24c52e313476e) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!16))

## 0.0.1 (2023-06-22)

### added (1 change)

- [Add output/buildtag file](gitlab-org/editor-extensions/gitlab-visual-studio-extension@e3ad6661962023498db90b7340f24c52e313476e) ([merge request](gitlab-org/editor-extensions/gitlab-visual-studio-extension!16))
