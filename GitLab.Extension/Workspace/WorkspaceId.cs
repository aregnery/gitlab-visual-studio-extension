using System;

namespace GitLab.Extension.Workspace
{
    public readonly struct WorkspaceId : IEquatable<WorkspaceId>
    {
        public string SolutionName { get; }
        public string SolutionPath { get; }

        public WorkspaceId(
            string solutionName,
            string solutionPath)
        {
            SolutionName = solutionName ?? throw new ArgumentNullException(nameof(solutionName));
            SolutionPath = solutionPath ?? throw new ArgumentNullException(nameof(solutionPath));
        }

        public bool Equals(
            WorkspaceId other)
        {
            return StringComparer.Ordinal.Equals(SolutionName, other.SolutionName) &&
                   StringComparer.Ordinal.Equals(SolutionPath, other.SolutionPath);
        }

        public override bool Equals(
            object obj)
        {
            return obj is WorkspaceId other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(
                SolutionName,
                SolutionPath);
        }

        public static bool operator ==(
            WorkspaceId left,
            WorkspaceId right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(
            WorkspaceId left,
            WorkspaceId right)
        {
            return !left.Equals(right);
        }

        public override string ToString()
        {
            return $"SolutionName: {SolutionName}, SolutionPath: {SolutionPath}";
        }
    }
}
