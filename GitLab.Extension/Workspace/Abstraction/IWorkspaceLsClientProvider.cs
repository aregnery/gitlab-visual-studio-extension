using GitLab.Extension.LanguageServer;

namespace GitLab.Extension.Workspace
{
    public interface IWorkspaceLsClientProvider
    {
        ILsClient GetClient(
            WorkspaceId workspaceId);
    }
}
