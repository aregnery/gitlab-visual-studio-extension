using System;

namespace GitLab.Extension.Workspace
{
    public interface IActiveWorkspaceStateObservable : IObservable<WorkspaceState>
    {
        WorkspaceState Current { get; }
    }
}
