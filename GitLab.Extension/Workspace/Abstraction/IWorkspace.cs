using System;

namespace GitLab.Extension.Workspace
{
    public interface IWorkspace : IDisposable
    {
        WorkspaceId Id { get; }
    }
}
