﻿using GitLab.Extension.Utility.Results;

namespace GitLab.Extension.Workspace
{
    public abstract class WorkspaceState
    {
        public static NoActiveWorkspace NoActiveWorkspace =>
            NoActiveWorkspace.Instance;

        public static WorkspaceInitializing WorkspaceInitializing(
            WorkspaceId workspaceId) =>
            new WorkspaceInitializing(workspaceId);

        public static WorkspaceInitializationFailed WorkspaceInitializationFailed(
            WorkspaceId workspaceId,
            IError error) =>
            new WorkspaceInitializationFailed(workspaceId, error);

        public static WorkspaceActive WorkspaceActive(
            IWorkspace workspace) =>
            new WorkspaceActive(workspace);
    }

    public class NoActiveWorkspace : WorkspaceState
    {
        private NoActiveWorkspace()
        {
        }

        public static NoActiveWorkspace Instance => new NoActiveWorkspace();
    }

    public class WorkspaceInitializationFailed : WorkspaceState
    {
        public WorkspaceInitializationFailed(
            WorkspaceId workspaceId,
            IError error)
        {
            WorkspaceId = workspaceId;
            Error = error;
        }

        public WorkspaceId WorkspaceId { get; }
        public IError Error { get; }
    }

    public class WorkspaceInitializing : WorkspaceState
    {
        public WorkspaceInitializing(WorkspaceId workspaceId)
        {
            WorkspaceId = workspaceId;
        }

        public WorkspaceId WorkspaceId { get; }
    }

    public class WorkspaceActive : WorkspaceState
    {
        public WorkspaceActive(IWorkspace workspace)
        {
            Workspace = workspace;
        }

        public IWorkspace Workspace { get; }

        public WorkspaceId WorkspaceId => Workspace.Id;
    }
}
