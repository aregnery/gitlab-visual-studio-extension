﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.Command
{
    public class CommandInitializer : ICommandInitializer
    {
        private readonly IEnumerable<ICommand> _commands;

        public CommandInitializer(
            IEnumerable<ICommand> commands)
        {
            _commands = commands;
        }

        public async Task InitializeAsync(
            AsyncPackage package)
        {
            foreach (var command in _commands)
            {
                await command.InitializeAsync(package);
            }
        }
    }
}
