﻿using System;
using Autofac;

namespace GitLab.Extension.Command
{
    public static class DependencyInjection
    {
        public static Type[] CommandTypes =
        {
            typeof(ToggleCodeSuggestionsCommand)
        };

        public static ContainerBuilder RegisterCommands(
            this ContainerBuilder builder)
        {
            // Register Initializer
            builder.RegisterType<CommandInitializer>()
                .As<ICommandInitializer>();

            // Register Commands
            foreach (var type in CommandTypes)
            {
                builder.RegisterType(type)
                    .As<ICommand>();
            }

            return builder;
        }
    }
}
