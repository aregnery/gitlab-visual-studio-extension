using System;
using GitLab.Extension.Utility.Results.Utilities;

namespace GitLab.Extension.Utility.Results.Errors
{
    public class ExceptionalError : Error
    {
        public Exception Exception { get; }
        
        public ExceptionalError(
            Exception exception)
            : this(exception.Message, exception) {}
        
        public ExceptionalError(
            string message,
            Exception exception) : base(message)
        {
            Exception = exception;
        }
        
        public override string ToString()
        {
            return new ReasonStringBuilder()
                .WithReasonType(GetType())
                .WithInfo(nameof(Message), Message)
                .WithInfo(nameof(Metadata), string.Join("; ", Metadata))
                .WithInfo(nameof(Reasons), string.Join("; ", Reasons))
                .WithInfo(nameof(Exception), Exception.ToString())
                .Build();
        }
    }
}
