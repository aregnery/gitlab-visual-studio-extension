using System;
using GitLab.Extension.Utility.Results.Errors;

namespace GitLab.Extension.Utility.Results
{
    public static class Result
    {
        public static Result<T> Ok<T>(T value) => Result<T>.Ok(value);
        public static Result<T> Error<T>(IError error) => Result<T>.Error(error);
        public static Result<T> Error<T>(string message) => Result<T>.Error(new Error(message));
        public static Result<T> Error<T>(string message, Exception ex) => Result<T>.Error(new ExceptionalError(message, ex));
        public static Result<T> Error<T>(Exception ex) => Result<T>.Error(new ExceptionalError(ex));
    }

    public readonly struct Result<T>
    {
        public static Result<T> Ok(T value) => new Result<T>(value);
        public static Result<T> Error(IError error) => new Result<T>(error);
        
        private readonly T _value;
        private readonly IError _error;
        private readonly bool _isSuccess;

        public bool IsError(out IError error)
        {
            error = _error;
            return !_isSuccess;
        }

        public bool IsSuccess(out T value)
        {
            value = _value;
            return _isSuccess;
        }

        private Result(
            T value)
        {
            _value = value;
            _error = null;
            _isSuccess = true;
        }

        private Result(
            IError error)
        {
            _value = default;
            _error = error;
            _isSuccess = false;
        }

        public T1 Match<T1>(
            Func<T, T1> ok, 
            Func<IError, T1> error)
        {
            return _isSuccess 
                ? ok(_value) 
                : error(_error);
        }
        
        public static implicit operator Result<T>(
            T value)
        {
            return Ok(value);
        }

        public static implicit operator Result<T>(
            Error error)
        {
            return Error(error);
        }
    }
}