﻿using System;

namespace GitLab.Extension.InfoBar
{
    public class InfoBarAction
    {
        public string Label { get; }
        public Action Action { get; }
        public bool IsButton { get; }

        public InfoBarAction(
            string label,
            Action action = null,
            bool isButton = false)
        {
            Label = label;
            Action = action;
            IsButton = isButton;
        }
    }
}
