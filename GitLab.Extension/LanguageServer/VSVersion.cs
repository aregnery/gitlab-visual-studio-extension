﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace GitLab.Extension.LanguageServer
{
    public static class VSVersion
    {
        static Version _vsVersion;
        static Version _osVersion;

        static VSVersion()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "msenv.dll");

            if (File.Exists(path))
            {
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(path);

                string verName = fvi.ProductVersion;

                CompanyName = Regex.Replace(fvi.CompanyName, @"[^\u0000-\u007F]+", string.Empty);
                ProductName = Regex.Replace(fvi.ProductName, @"[^\u0000-\u007F]+", string.Empty);

                for (int i = 0; i < verName.Length; i++)
                {
                    if (!char.IsDigit(verName, i) && verName[i] != '.')
                    {
                        verName = verName.Substring(0, i);
                        break;
                    }
                }
                _vsVersion = new Version(verName);
            }
            else
                _vsVersion = new Version(0, 0); // Not running inside Visual Studio!

            FullVersion = _vsVersion;
        }

        public static Version FullVersion { get; }

        public static string ProductName { get; }
        public static string CompanyName { get; }

        public static Version OSVersion
        {
            get { return _osVersion ?? (_osVersion = Environment.OSVersion.Version); }
        }

        public static bool VS2012OrLater
        {
            get { return FullVersion >= new Version(11, 0); }
        }

        public static bool VS2010OrLater
        {
            get { return FullVersion >= new Version(10, 0); }
        }

        public static bool VS2008OrOlder
        {
            get { return FullVersion < new Version(9, 0); }
        }

        public static bool VS2005
        {
            get { return FullVersion.Major == 8; }
        }

        public static bool VS2008
        {
            get { return FullVersion.Major == 9; }
        }

        public static bool VS2010
        {
            get { return FullVersion.Major == 10; }
        }

        public static bool VS2012
        {
            get { return FullVersion.Major == 11; }
        }
    }
}
