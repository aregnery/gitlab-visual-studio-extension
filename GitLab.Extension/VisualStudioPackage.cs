﻿using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Events;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reactive.Disposables;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;
using GitLab.Extension.SettingsUtil;
using Serilog;
using Autofac;
using GitLab.Extension.CodeSuggestions.State;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.Command;
using GitLab.Extension.Workspace;

namespace GitLab.Extension
{
    /// <summary>
    /// Listen for solution open/closed events and perform any actions needed.
    /// </summary>
    /// <remarks>
    /// Handle extension initialization:
    /// 
    /// 1. Start logging and add output window
    /// 2. Display status bar icon
    /// 3. Start a language server for this solution
    /// </remarks>
    [PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
    [Guid(PackageIds.GitLabPackageIdString)]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    // This autoload doesn't happen when opening a solution file directly.
    [ProvideAutoLoad(VSConstants.UICONTEXT.SolutionOpening_string, PackageAutoLoadFlags.BackgroundLoad)]
    // This autoload occurs after Visual Studio has completely started. It would be nice
    // if the autoload would occur earlier.
    [ProvideAutoLoad(VSConstants.UICONTEXT.ShellInitialized_string, PackageAutoLoadFlags.BackgroundLoad)]
    public sealed class VisualStudioPackage : AsyncPackage
    {
        private readonly ISettings _settings;
        private readonly ILsClientManager _lsClientManager;
        private readonly ILsProcessManager _lsProcessManager;
        private readonly ICommandInitializer _commandInitializer;
        private readonly Status.StatusBar _statusBar;

        private readonly CompositeDisposable _subscriptions =
            new CompositeDisposable();

        private readonly Lazy<IWorkspaceEventHandler> _workspaceEventHandler =
            new Lazy<IWorkspaceEventHandler>(() =>
                DependencyInjection.Instance.Scope.Resolve<IWorkspaceEventHandler>());

        private bool _initialized = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualStudioPackage"/> class.
        /// </summary>
        public VisualStudioPackage()
        {
            // Inside this method you can place any initialization code that does not require
            // any Visual Studio service because at this point the package object is created but
            // not sited yet inside Visual Studio environment. The place to do all the other
            // initialization is the Initialize method.

            _lsClientManager = DependencyInjection.Instance.Scope.Resolve<ILsClientManager>();
            _lsProcessManager = DependencyInjection.Instance.Scope.Resolve<ILsProcessManager>();
            _settings = DependencyInjection.Instance.Scope.Resolve<ISettings>();
            _statusBar = DependencyInjection.Instance.Scope.Resolve<Status.StatusBar>();
            _commandInitializer = DependencyInjection.Instance.Scope.Resolve<ICommandInitializer>();
        }

        protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            
            // Configure logging. This will create the output window in Visual Studio
            Logging.ConfigureLogging();
            
            // Run this code in the background to avoid VS notifying the user a background
            // task is still running. Also makes VS start faster than if it was inline.
            var task = new Func<Task>(async () => {
                if (_initialized)
                    return;

                // Since this package might not be initialized until after a solution has finished loading,
                // we need to check if a solution has already been loaded and then handle it.
                bool isSolutionLoaded = await IsSolutionLoadedAsync();

                if (isSolutionLoaded)
                {
                    HandleAfterOpenSolution();
                }

                // Listen for subsequent solution events
                SolutionEvents.OnAfterOpenSolution += HandleAfterOpenSolution;
                SolutionEvents.OnBeforeCloseSolution += HandleBeforeCloseSolution;

                _initialized = true;
            });

            InitializeStatusBarSubscription();
            
            await _commandInitializer.InitializeAsync(this);
            await task.Invoke();
        }

        private void InitializeStatusBarSubscription()
        {
            var subscription =
                DependencyInjection
                    .Instance.Scope.Resolve<ICodeSuggestionsEnabledStateProvider>()
                    .CodeSuggestionEnabledStateObservable
                    .Subscribe(_statusBar.Render);
            
            _subscriptions.Add(subscription);
        }

        private string GetSolutionName(string solutionFullName)
        {
            var solutionName = solutionFullName;

            if (solutionName.ToLower().EndsWith(".sln"))
                solutionName = Path.GetFileNameWithoutExtension(solutionName);

            return solutionName;
        }

        private void HandleBeforeCloseSolution(object sender, EventArgs e)
        {
            if (!_settings.Configured)
                return;

            Log.Debug($"{nameof(VisualStudioPackage)}.{nameof(HandleBeforeCloseSolution)}");

            var l = new Func<Task>(async () =>
            {
                await JoinableTaskFactory.SwitchToMainThreadAsync();

                var dte2 = (DTE2)GetGlobalService(typeof(SDTE));
                if (dte2 == null)
                    return;

                var solutionName = GetSolutionName(dte2.Solution.FullName);
                var solutionPath = Path.GetDirectoryName(dte2.Solution.FileName);

                await _lsClientManager.DisposeClientAsync(solutionName);
                await _lsProcessManager.StopLanguageServerAsync(solutionPath);
                
                _workspaceEventHandler.Value.OnWorkspaceClose(
                    new WorkspaceId(solutionName, solutionPath));
            });

            _ = l.Invoke();
        }

        private async Task<bool> IsSolutionLoadedAsync()
        {
            await JoinableTaskFactory.SwitchToMainThreadAsync();
            var solService = await GetServiceAsync(typeof(SVsSolution)) as IVsSolution;

            if (solService == null)
                return false;

            ErrorHandler.ThrowOnFailure(solService.GetProperty((int)__VSPROPID.VSPROPID_IsSolutionOpen, out object value));

            return value is bool isSolOpen && isSolOpen;
        }

        private void HandleAfterOpenSolution(object sender = null, EventArgs e = null)
        {
            // Handle the open solution and try to do as much work
            // on a background thread as possible

            Log.Debug($"{nameof(VisualStudioPackage)}.{nameof(HandleAfterOpenSolution)}");

            var l = new Func<Task>(async () =>
            {
                await JoinableTaskFactory.SwitchToMainThreadAsync();
                
                Log.Debug($"{nameof(VisualStudioPackage)}.{nameof(HandleAfterOpenSolution)}");

                // Display status bar icon
                _statusBar.InitializeDisplay();

                // Start the language server, but only if we are configured
                if (!_settings.Configured)
                {
                    Log.Warning("This extension will not function until it's configured: Tools -> Options -> GitLab");
                    return;
                }

                var dte2 = (DTE2)GetGlobalService(typeof(SDTE));
                if (dte2 == null)
                    return;

                var solutionName = GetSolutionName(dte2.Solution.FullName);
                var solutionPath = Path.GetDirectoryName(dte2.Solution.FileName);

                var lsClient = _lsClientManager.GetClient(solutionName, solutionPath);
                await lsClient.ConnectAsync();
                
                _workspaceEventHandler.Value.OnWorkspaceOpen(
                    new WorkspaceId(solutionName, solutionPath));
            });

            _ = l.Invoke();
        }

        protected override void Dispose(
            bool disposing)
        {
            base.Dispose(disposing);
            _subscriptions.Dispose();
        }
    }
}
