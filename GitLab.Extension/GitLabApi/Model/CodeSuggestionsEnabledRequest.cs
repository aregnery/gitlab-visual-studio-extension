﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace GitLab.Extension.GitLabApi.Model
{
    public class CodeSuggestionsEnabledRequest
    {
        [JsonProperty("project_path")]
        [JsonPropertyName("project_path")]
        public string ProjectPath { get; set; }
    }
}