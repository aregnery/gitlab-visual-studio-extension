﻿using System;
using System.Text.Json.Serialization;
using GitLab.Extension.GitLabApi.Serialization;

namespace GitLab.Extension.GitLabApi.Model
{
    [JsonConverter(typeof(GitLabVersionJsonConverter))]
    public class GitLabVersion : IComparable<GitLabVersion>, IEquatable<GitLabVersion>
    {
        private const char VersionSeparatorChar = '.';
        private const char PreReleaseSeparatorChar = '-';
        
        public uint Major { get; }
        public uint Minor { get; }
        public uint Patch { get; }
        public string PreRelease { get; }

        public GitLabVersion(
            uint major = 0,
            uint minor = 0,
            uint patch = 0,
            string preRelease = null)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
            PreRelease = preRelease;
        }
        
        public static GitLabVersion Parse(
            string version)
        {
            if (string.IsNullOrEmpty(version))
            {
                throw new ArgumentNullException(nameof(version));
            }
            
            var splitVersion = version.Split(PreReleaseSeparatorChar);
            var versionNumbers = splitVersion[0].Split(VersionSeparatorChar);

            if (versionNumbers.Length < 3)
                throw new ArgumentException(
                    "Version string does not contain all required components (Major.Minor.Patch).");

            return new GitLabVersion(
                major: uint.Parse(versionNumbers[0]),
                minor: uint.Parse(versionNumbers[1]),
                patch: uint.Parse(versionNumbers[2]),
                preRelease: splitVersion.Length > 1
                    ? splitVersion[1]
                    : null);
        }

        public int CompareTo(GitLabVersion other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;

            // compare major
            var majorComparison = Major.CompareTo(other.Major);
            if (majorComparison != 0) return majorComparison;
            
            // compare minor
            var minorComparison = Minor.CompareTo(other.Minor);
            if (minorComparison != 0) return minorComparison;
            
            // compare patch
            var patchComparison = Patch.CompareTo(other.Patch);
            if (patchComparison != 0) return patchComparison;
            
            // version without pre-release part is always greater than one with with a pre-release part
            if (string.IsNullOrEmpty(PreRelease) && !string.IsNullOrEmpty(other.PreRelease))
                return 1;
            
            if (!string.IsNullOrEmpty(PreRelease) && string.IsNullOrEmpty(other.PreRelease))
                return -1;
            
            // both have pre-release parts
            return string.Compare(PreRelease, other.PreRelease, StringComparison.Ordinal);
        }

        public bool Equals(GitLabVersion other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Major == other.Major && Minor == other.Minor && Patch == other.Patch && PreRelease == other.PreRelease;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((GitLabVersion)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Major, Minor, Patch, PreRelease);
        }

        public override string ToString()
        {
            var version = $"{Major}.{Minor}.{Patch}";
            if (!string.IsNullOrEmpty(PreRelease))
            {
                version += $"-{PreRelease}";
            }

            return version;
        }

        public static bool operator <(GitLabVersion left, GitLabVersion right)
        {
            return left is null ? right is null : left.CompareTo(right) < 0;
        }
        
        public static bool operator >(GitLabVersion left, GitLabVersion right)
        {
            return right < left;
        }
        
        public static bool operator <=(GitLabVersion left, GitLabVersion right)
        {
            return left is null || left.CompareTo(right) <= 0;
        }

        public static bool operator >=(GitLabVersion left, GitLabVersion right)
        {
            return right <= left;
        }

        public static implicit operator GitLabVersion(
            string version)
        {
            return Parse(version);
        }
    }
}