﻿using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Utility.Results;

namespace GitLab.Extension.GitLabApi.Extensions
{
    public static class GitLabClientFactoryExtensions
    {
        public static Result<IGitLabClient> CreateClient(
            this IGitLabClientFactory gitLabClientFactory,
            ISettings settings)
        {
            return GitLabApiSettings
                .Create(
                    settings.GitLabUrl,
                    settings.GitLabAccessToken)
                .Map(gitLabClientFactory.CreateClient);
        }
    }
}