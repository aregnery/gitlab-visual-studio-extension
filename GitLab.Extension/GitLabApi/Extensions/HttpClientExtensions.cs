﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.GitLabApi.Errors;
using GitLab.Extension.Utility.Results;

namespace GitLab.Extension.GitLabApi.Extensions
{
    public static class HttpClientExtensions
    {
        public static async Task<Result<HttpResponseMessage>> SendAsync(
            this HttpClient httpClient,
            string requestUri,
            HttpMethod method,
            HttpContent content = null,
            CancellationToken cancellationToken = default)
        {
            try
            {
                var httpRequest = new HttpRequestMessage(method, requestUri) { Content = content };
                var response = await httpClient.SendAsync(httpRequest, cancellationToken);

                if (!response.IsSuccessStatusCode)
                {
                    return Result.Error<HttpResponseMessage>(
                        GetErrorFromResponse(response));
                }

                return Result.Ok(response);
            }
            catch (HttpRequestException ex)
            {
                return Result.Error<HttpResponseMessage>(
                    new GitLabApiError(message: ex.Message));
            }
        }

        public static async Task<Result<TResponse>> SendAsync<TResponse>(
            this HttpClient httpClient,
            string requestUri,
            HttpMethod method,
            HttpContent content = null,
            CancellationToken cancellationToken = default)
        {
            var responseResult =
                await SendAsync(
                    httpClient,
                    requestUri,
                    method,
                    content,
                    cancellationToken);

            return await responseResult
                .Map(httpResponseMessage =>
                    httpResponseMessage.Content.ReadFromJsonAsync<TResponse>(
                        cancellationToken));
        }

        public static Task<Result<HttpResponseMessage>> SendAsJsonAsync<TRequest>(
            this HttpClient httpClient,
            string requestUri,
            HttpMethod method,
            TRequest request,
            CancellationToken cancellationToken = default)
        {
            var content = JsonContent.Create(request, options: Constants.GitLabJsonSerializerOptions);
            return SendAsync(httpClient, requestUri, method, content, cancellationToken);
        }

        public static async Task<Result<TResponse>> SendAsJsonAsync<TRequest, TResponse>(
            this HttpClient httpClient,
            string requestUri,
            HttpMethod method,
            TRequest request,
            CancellationToken cancellationToken = default)
        {
            var content = JsonContent.Create(request, options: Constants.GitLabJsonSerializerOptions);
            var responseResult = await httpClient.SendAsync(requestUri, method, content, cancellationToken);
            return await responseResult.Bind(async httpResponseMessage =>
            {
                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    return Result.Error<TResponse>(
                        GetErrorFromResponse(httpResponseMessage));
                }

                var response = await httpResponseMessage.Content.ReadFromJsonAsync<TResponse>(cancellationToken);

                return Result.Ok(response);
            });
        }

        public static async Task<Result<TResponse>> SendGraphQlQueryAsync<TResponse>(
            this HttpClient httpClient,
            string query,
            CancellationToken cancellationToken = default)
            where TResponse : class
        {
            const string requestUri = "api/graphql";

            var request = new GraphQlRequest
            {
                Query = query
            };

            return await SendAsJsonAsync<GraphQlRequest, TResponse>(
                httpClient,
                requestUri,
                HttpMethod.Post,
                request,
                cancellationToken);
        }

        private static GitLabApiError GetErrorFromResponse(
            HttpResponseMessage response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.Unauthorized:
                    return new UnauthorizedError();
                default:
                    return new GitLabApiError(
                        response.StatusCode,
                        $"Request failed with status code {response.StatusCode}");
            }
        }

        private class GraphQlRequest
        {
            public object Query { get; set; }
        }
    }
}