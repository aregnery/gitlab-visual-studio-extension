using GitLab.Extension.Workspace;

namespace GitLab.Extension.CodeSuggestions.Model
{
    public readonly struct GitLabProposalMetadata
    {
        public GitLabProposalMetadata(
            WorkspaceId workspaceId,
            string telemetryTrackingId)
        {
            WorkspaceId = workspaceId;
            TelemetryTrackingId = telemetryTrackingId;
        }

        /// <summary>
        /// The WorkspaceId associated with the proposal.
        /// </summary>
        public WorkspaceId WorkspaceId { get; }
        
        /// <summary>
        /// The Telemetry ID used for tracking the proposal's usage and behavior
        /// </summary>
        public string TelemetryTrackingId { get; }
    }
}
