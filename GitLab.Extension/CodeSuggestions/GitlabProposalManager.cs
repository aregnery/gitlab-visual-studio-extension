﻿using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Tagging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GitLab.Extension.CodeSuggestions
{
    internal class GitlabProposalManager : ProposalManagerBase
    {
        private readonly GitlabProposalManagerProvider _factory;
        private ITagAggregator<IClassificationTag> _tagger;
        private readonly ITextView _view;
        private readonly Language _language;

        /// <summary>
        /// Characters to trigger suggestions on for all languages
        /// </summary>
        private static readonly IReadOnlyList<char> BaseTriggerCharacters = new char[]
        {
            ' ',
            '\t',
            '.',
            '=',
            ',',
            '(',
            '[',
            '"',
            '\'',
        };

        /// <summary>
        /// Trigger characters for ITextView ContentType. 
        /// This includes BaseTriggerCharacters along with any language specific ones.
        /// </summary>
        private readonly List<char> _triggerCharacters = new List<char>(20);

        /// <summary>
        /// Tag classifications to skip. For example, skip suggestions for comments.
        /// </summary>
        private static readonly IReadOnlyList<string> _classificationsToSkip = new string[]
        {
        };

        public GitlabProposalManager(ITextView view, GitlabProposalManagerProvider factory, Language language)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            _view = view;
            _factory = factory;
            _tagger = factory.ViewTagAggregatorFactoryService.CreateTagAggregator<IClassificationTag>(view);
            _language = language;

            _triggerCharacters.AddRange(BaseTriggerCharacters);
            _triggerCharacters.AddRange(language.TriggerCharacters);
        }

        public override async Task DisposeAsync()
        {
            if (_tagger == null)
                return;

            var tagger = _tagger;
            _tagger = null;

            await _factory.JoinableTaskFactory.SwitchToMainThreadAsync(VsShellUtilities.ShutdownToken);
            tagger.Dispose();
            tagger = null;

            await base.DisposeAsync();

            GC.KeepAlive(this);
        }

        public override bool TryGetIsProposalPosition(
          VirtualSnapshotPoint caret,
          ProposalScenario scenario,
          char triggerCharacter,
          ref bool value)
        {
            ThreadHelper.ThrowIfNotOnUIThread(nameof(TryGetIsProposalPosition));
            value = false;

            if (scenario == ProposalScenario.Return)
            {
                var position = caret.Position;
                var containingLine = ((SnapshotPoint)position).GetContainingLine();
                if (containingLine.LineNumber >= 1)
                {
                    var snapshot = containingLine.Snapshot;
                    var line = containingLine.Snapshot.GetLineFromLineNumber(containingLine.LineNumber - 1);
                    var end = line.End;
                    var span = Span.FromBounds(Math.Max(0, end.Position - 1), caret.Position);

                    value = CheckClassification(new SnapshotSpan(snapshot, span));
                    return value;
                }
            }
            else if (scenario == ProposalScenario.ExplicitInvocation)
            {
                value = CheckTriggerCharAndClassification(caret.Position, true);
                return value;
            }
            else if (scenario == ProposalScenario.TypeChar)
            {
                value = CheckTriggerCharAndClassification(caret.Position, false);
                return value;
            }
            else if (scenario != ProposalScenario.CaretMove)
            {
                if (caret.IsInVirtualSpace)
                {
                    value = false;
                    return value;
                }

                var position = caret.Position;
                if (position.Position > 0)
                {
                    if (CheckClassification(new SnapshotSpan(caret.Position - 1, 1)))
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
                else
                {
                    value = true;
                }
            }

            return value;
        }

        /// <summary>
        /// Check the if the trigger character is in our list of trigger characters.
        /// Also checks classification.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="ignoreTrigger"></param>
        /// <returns>True if a suggestion should be shown</returns>
        private bool CheckTriggerCharAndClassification(SnapshotPoint position, bool ignoreTrigger)
        {
            if (position.Position <= 0)
                return false;

            var trigger = position - 1;
            var triggerCharacter = trigger.GetChar();

            if (ignoreTrigger)
            {
                var twobackPosition = position.Position - 2;
                var start = position.GetContainingLine().Start;
                var startPosition = start.Position;
                var spanStartPosition = Math.Max(twobackPosition, startPosition);

                var spanToCheck = new SnapshotSpan(
                        position.Snapshot,
                        Span.FromBounds(spanStartPosition, position.Position));

                return CheckClassification(spanToCheck);
            }

            if (_triggerCharacters.Contains<char>(triggerCharacter) && !OnlyLeadingWhiteSpaceOnLine(trigger))
            {
                // When the trigger character is whitespace,
                // only provide suggestion if a non-whitespace character
                // occurs immediatly prior to the current position
                // and has an acceptable classification tag.
                if (char.IsWhiteSpace(triggerCharacter))
                {
                    var snapshotPoint = trigger - 1;
                    if (!char.IsWhiteSpace(snapshotPoint.GetChar()))
                    {
                        var twobackPosition = position.Position - 2;
                        var start = position.GetContainingLine().Start;
                        var startPosition = start.Position;
                        var spanStartPosition = Math.Max(twobackPosition, startPosition);

                        var spanToCheck = new SnapshotSpan(
                                position.Snapshot,
                                Span.FromBounds(spanStartPosition, position.Position));

                        return CheckClassification(spanToCheck);
                    }
                }
                // otherwise, check the a span starting two positions back
                // for classification.
                else
                {
                    var twobackPosition = position.Position - 2;
                    var start = position.GetContainingLine().Start;
                    var startPosition = start.Position;
                    var spanStartPosition = Math.Max(twobackPosition, startPosition);

                    var spanToCheck = new SnapshotSpan(
                            position.Snapshot,
                            Span.FromBounds(spanStartPosition, position.Position));

                    return CheckClassification(spanToCheck);
                }
            }

            return false;
        }

        /// <summary>
        /// Check the classification of a provided SnapshotSpan
        /// </summary>
        /// <param name="span"></param>
        /// <returns>True if the classification is not in _classificationsToSkip</returns>
        private bool CheckClassification(SnapshotSpan span)
        {
            if (_tagger == null)
                return false;

            return !_tagger.GetTags(span)
                .Select(x => x.Tag.ClassificationType.Classification)
                .Intersect(_classificationsToSkip)
                .Any();
        }

        /// <summary>
        /// Check if the SnapshotPoint's containing line has all whitespace before it.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns>True if containing line is all whitespace prior to our trigger point.</returns>
        private static bool OnlyLeadingWhiteSpaceOnLine(SnapshotPoint trigger)
        {
            var containingLine = trigger.GetContainingLine();

            for (var pos = containingLine.Start.Position; pos < trigger.Position; pos++)
            {
                if (!char.IsWhiteSpace(trigger.Snapshot[pos]))
                    return false;
            }

            return true;
        }
    }
}
