using System;

namespace GitLab.Extension.CodeSuggestions.State
{
    public interface ICodeSuggestionsEnabledValidator
    {
        IObservable<CodeSuggestionsEnabledState> ValidationResultObservable { get; }
    }
}
