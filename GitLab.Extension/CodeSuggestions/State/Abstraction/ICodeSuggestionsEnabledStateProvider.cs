using System;

namespace GitLab.Extension.CodeSuggestions.State
{
    public interface ICodeSuggestionsEnabledStateProvider
    {
        IObservable<CodeSuggestionsEnabledState> CodeSuggestionEnabledStateObservable { get; }
        IObservable<bool> IsCodeSuggestionsEnabledObservable { get; }
        CodeSuggestionsEnabledState CodeSuggestionsEnabledState { get; }
        bool IsCodeSuggestionsEnabled { get; }
    }
}
