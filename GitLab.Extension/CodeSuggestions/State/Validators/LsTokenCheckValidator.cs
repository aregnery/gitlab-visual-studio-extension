using System;
using System.Reactive.Linq;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.SettingsUtil;

namespace GitLab.Extension.CodeSuggestions.State.Validators
{
    public class LsTokenCheckValidator : ICodeSuggestionsEnabledValidator, IDisposable
    {
        private readonly IDisposable _subscription;

        public IObservable<CodeSuggestionsEnabledState> ValidationResultObservable { get; }

        public LsTokenCheckValidator(
            IObservable<ISettings> settingsObservable,
            IObservable<TokenValidationNotification> tokenValidationObservable)
        {
            var disabledOnTokenValidation =
                tokenValidationObservable
                    .Select(_ => CodeSuggestionsEnabledState.Disabled(Constants.Messages.InvalidAccessToken));

            var enabledOnSettingsChangedObservable =
                settingsObservable
                    .Select(x => CodeSuggestionsEnabledState.Enabled);

            var observable =
                enabledOnSettingsChangedObservable
                    .Merge(disabledOnTokenValidation)
                    .StartWith(CodeSuggestionsEnabledState.Enabled)
                    .Replay(1);

            _subscription = observable.Connect();
            ValidationResultObservable = observable.AsObservable();
        }

        public void Dispose()
        {
            _subscription?.Dispose();
        }
    }
}
