using System.Linq;
using GitLab.Extension.GitLabApi.Errors;
using GitLab.Extension.Utility.Results;

namespace GitLab.Extension.CodeSuggestions.State.Utilities
{
    public static class CodeSuggestionEnabledStateUtilities
    {
        public static CodeSuggestionsEnabledState CreateCodeSuggestionStateFromError(
            IError error)
        {
            var message = error switch
            {
                UnauthorizedError _ => Constants.Messages.Unauthorized,
                GitLabApiConfigurationError gitLabApiConfigurationError => 
                    gitLabApiConfigurationError.Reasons.FirstOrDefault() switch
                    {
                        InvalidAccessToken _ => Constants.Messages.InvalidAccessToken,
                        InvalidBaseUrl _ => Constants.Messages.InvalidConfiguredUrl,
                        _ => Constants.Messages.ConfigurationFailed
                    },
                _ => Constants.Messages.GeneralError
            };
            
            return CodeSuggestionsEnabledState.Disabled(message, true);
        }
    }
}
