﻿using System;
using System.Collections.Generic;
using System.Linq;
using GitLab.Extension.LanguageServer;

namespace GitLab.Extension.CodeSuggestions
{
    /// <summary>
    /// Defines all the supported languages and provides
    /// helper methods for determining if a feature can
    /// be provided to a specific language.
    /// </summary>
    public class LanguageManager : ILanguageManager
    {
        private IReadOnlyList<Language> _languages;

        public LanguageManager()
        {
            _languages = new Language[]
            {
                new Language("C", "c", LsLanguageId.C, new [] {"c", "h"}, new[] {'/', '{' }),
                new Language("C++", "cpp", LsLanguageId.CPP, new []{"cpp", "hpp"}, new[] {'/', '{' }),
                new Language("C++", "c/c++", LsLanguageId.CPP, new []{"cpp", "hpp"}, new[] {'/', '{' }),
                new Language("C#", "csharp", LsLanguageId.CSharp, new string[0], new[] {'/', '{' }),
                new Language("CSS", "css", LsLanguageId.CSS, new [] {"css"}, new[] { '/' }),
                new Language("CUDA C++", "cuda-cpp", LsLanguageId.CudaCpp, new string[0], new[] {'/', '{' }),
                new Language("Golang", "go", LsLanguageId.Go, new [] {"go"}, new [] {'/', '{' }),
                new Language("HTML", "html", LsLanguageId.HTML, new [] {"html"}, new [] {'<', '-' }),
                new Language("Java", "java", LsLanguageId.Java, new [] {"java"}, new [] {'/', '{' }),
                new Language("JavaScript", "javascript", LsLanguageId.JavaScript, new string[0], new[] {'/' , '{' }),
                new Language("JavaScript React", "javascriptreact", LsLanguageId.JavaScriptReact, new string[0], new[] {'/' , '{' }),
                new Language("Kotlin", "kotlin", LsLanguageId.Kotlin, new [] { "kt" }, new[] { '/' , '{' }),
                new Language("Markdown", "markdown", LsLanguageId.Markdown, new[] { "md" }, new [] {'<', '-' }),
                new Language("Objective-C", "objective-c", LsLanguageId.ObjectiveC, new string[0], new[] { '/', '{' }),
                new Language("Objective-C++", "objective-cpp", LsLanguageId.ObjectiveCpp, new string[0], new[] { '/', '{' }),
                new Language("PHP", "php", LsLanguageId.Php, new string[0], new[] { '/', '#' }),
                new Language("Python", "python", LsLanguageId.Python, new string[0], new[] {'#', ':' }),
                new Language("Ruby", "ruby", LsLanguageId.Ruby, new string[0], new[] {'#' }),
                new Language("Rust", "rust", LsLanguageId.Rust, new [] {"rs"}, new[] { '/', '{' }),
                new Language("Shell Script", "shellscript", LsLanguageId.ShellScript, new [] {"sh"}, new[] { '#' }),
                new Language("SQL", "sql", LsLanguageId.SQL, new [] {"sql"}, new char[0]),
                new Language("Scala", "scala", LsLanguageId.Scala, new string[0], new [] {'/' }),
                new Language("Swift", "swift", LsLanguageId.Swift, new [] {"swift" }, new[] { '/' }),
                new Language("TypeScript", "typescript", LsLanguageId.TypeScript, new string[0], new[] { '/', '{' }),
                new Language("TypeScript React", "typescriptreact", LsLanguageId.TypeScriptReact, new string[0], new[] { '/', '{' }),
            };
        }

        public IReadOnlyList<Language> Languages
        {
            get { return _languages; }
            set { _languages = value; }
        }

        public string GetExtensionFromFilename(string filename)
        {
            if (string.IsNullOrWhiteSpace(filename))
                return "";

            var periodIndex = filename.LastIndexOf('.');
            if (periodIndex == -1 || periodIndex+1 >= filename.Length)
                return "";

            return filename.Substring(periodIndex + 1);
        }

        private string FixContentType(string contentType)
        {
            contentType = contentType.ToLower();

            if (contentType.StartsWith("code++."))
                contentType = contentType.Substring("code++.".Length);

            return contentType;
        }

        public bool CheckFeatureCodeSuggestion(string contentType, string extension)
        {
            contentType = FixContentType(contentType);

            var ret = _languages
                .Any(x => x.FeatureCodeSuggestions == true && 
                     (x.ContentType == contentType || x.Extensions.Contains(extension)));

            return ret;
        }

        public Language GetLanguage(string contentType, string extension)
        {
            contentType = FixContentType(contentType);

            var ret = _languages
                .FirstOrDefault(x => x.ContentType == contentType || x.Extensions.Contains(extension));

            return ret;
        }
    }
}
